package com.example.habb.pharmablock;

public class Adapter {

    String Actor ;
    String Contact ;
    String Drug_id ;
    String Drug_name ;
    String Full_name ;
    String Institution ;
    String Location;

    public Adapter(String actor, String contact, String drug_id, String drug_name, String full_name, String institution, String location) {
        Actor = actor;
        Contact = contact;
        Drug_id = drug_id;
        Drug_name = drug_name;
        Full_name = full_name;
        Institution = institution;
        Location = location;
    }

    public String getActor() {
        return Actor;
    }

    public void setActor(String actor) {
        Actor = actor;
    }

    public String getContact() {
        return Contact;
    }

    public void setContact(String contact) {
        Contact = contact;
    }

    public String getDrug_id() {
        return Drug_id;
    }

    public void setDrug_id(String drug_id) {
        Drug_id = drug_id;
    }

    public String getDrug_name() {
        return Drug_name;
    }

    public void setDrug_name(String drug_name) {
        Drug_name = drug_name;
    }

    public String getFull_name() {
        return Full_name;
    }

    public void setFull_name(String full_name) {
        Full_name = full_name;
    }

    public String getInstitution() {
        return Institution;
    }

    public void setInstitution(String institution) {
        Institution = institution;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }
}
