package com.example.habb.pharmablock;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class UploadActorData extends AppCompatActivity {


    private EditText mActor,mContact,mDrugId,mFullname,mInstitution,mLocation,mDrugName;
    private ProgressBar mProgressBar;
    Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_actor_data);
        getWindow().setBackgroundDrawableResource(R.drawable.back_a);


        mActor=findViewById(R.id.actor);
        mContact=findViewById(R.id.contact);
        mDrugId=findViewById(R.id.drug_ID);
        mFullname=findViewById(R.id.full_name);
        mInstitution=findViewById(R.id.institution);
        mLocation=findViewById(R.id.location);
        mDrugName=findViewById(R.id.drug_Name);
        mProgressBar=findViewById(R.id.progressBar3);

        mProgressBar.setVisibility(View.INVISIBLE);
    }

    public void sendData(View view){



        final String actor=mActor.getText().toString().trim();
        final String fullName=mFullname.getText().toString().trim();
        final String drugId=mDrugId.getText().toString().trim();
        final String contact=mContact.getText().toString().trim();
        final String institution=mInstitution.getText().toString().trim();
        final String location=mLocation.getText().toString().trim();
        final String drugName=mDrugName.getText().toString().trim();

        if( mActor.getText().equals("")){
            Toast.makeText(this, "Input details", Toast.LENGTH_SHORT).show();
        }else {

            final String url = "https://blockchain0.herokuapp.com/new_transaction";


            RequestQueue queue = Volley.newRequestQueue(this);


            StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject jsonObject1 = jsonObject.getJSONObject("transaction");
                                String id = jsonObject1.getString("_id");

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            Log.i("bhimmmmmmmm", response);
                            handler = new Handler();
                            mProgressBar.setVisibility(View.VISIBLE);
                            Runnable r = new Runnable() {
                                public void run() {

                                    mProgressBar.setVisibility(View.INVISIBLE);
                                }
                            };

                            handler.postDelayed(r, 2500);

                            Toast.makeText(UploadActorData.this, "Upload into chain Successful", Toast.LENGTH_LONG).show();

                            mProgressBar.setVisibility(View.INVISIBLE);

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            Toast.makeText(UploadActorData.this, "Unsuccess", Toast.LENGTH_LONG).show();
                            error.printStackTrace();
                            Log.i("Error", error.toString());
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                                //This indicates that the request has either time out or there is no connection
                                Toast.makeText(getApplicationContext(), "Check your internet and try again!", Toast.LENGTH_LONG).show();

                            } else if (error instanceof AuthFailureError) {
                                //Error indicating that there was an Authentication Failure while performing the request
                                Toast.makeText(getApplicationContext(), "This Email is already registered, please use a different one", Toast.LENGTH_LONG).show();

                            } else if (error instanceof ServerError) {
                                //Indicates that the server responded with a error response
                                Toast.makeText(getApplicationContext(), "Server error! Try again later", Toast.LENGTH_LONG).show();

                            } else if (error instanceof NetworkError) {
                                //Indicates that there was network error while performing the request
                                Toast.makeText(getApplicationContext(), "Network error", Toast.LENGTH_LONG).show();

                            } else // Indicates that the server response could not be parsed
                                if (error instanceof ParseError)
                                    Toast.makeText(getApplicationContext(), "Parse Error", Toast.LENGTH_LONG).show();

                        }


                    }) {


                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("actor", actor);
                    params.put("contact", contact);
                    params.put("drug_id", drugId);
                    // postparams.put("lastName", "ka4");
                    params.put("drug_name", drugName);
                    params.put("full_name", fullName);
                    params.put("institution", institution);
                    params.put("location", location);
                    return params;


                }
            };

            queue.add(postRequest);
        }

    }

    public void sendDta(View view){

            final String url = "https://blockchain0.herokuapp.com/get_chain";

            RequestQueue queue = Volley.newRequestQueue(this);

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Toast.makeText(UploadActorData.this, "REsponse: " + response, Toast.LENGTH_SHORT).show();
                    Log.i("Suckess", response);


                    try {
                        JSONObject arr = new JSONObject(response);
                        JSONArray jObj = arr.getJSONArray("chain");
                        for (int i = 0; i < jObj.length(); i++) {
                            JSONObject mJsonObject = jObj.getJSONObject(i);
                            Log.i("trans2", mJsonObject.toString());

                            Log.d("OutPut", mJsonObject.getString("transactions"));
                        }
                        Log.i("trans1", jObj.toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }) {

            };
            queue.add(stringRequest);









       /*  StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("actor", "Manufacturer");
                params.put("contact", "0248085412");
                params.put("drug_id", "001");
                // postparams.put("lastName", "kad");
                params.put("drug_name", "Paracetamol");
                params.put("full_name","John Deo");
                params.put("institution", "Ernest chemis");
                params.put("location", "Accra");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        queue.add(postRequest);*/


    }



}
