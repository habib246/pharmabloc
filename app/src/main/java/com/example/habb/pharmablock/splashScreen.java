package com.example.habb.pharmablock;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import java.lang.reflect.Method;

public class splashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

      Handler handler = new Handler();
         Runnable r = new Runnable() {
            public void run() {

                Intent intent=new Intent(splashScreen.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        };

        handler.postDelayed(r, 2500);

    }
}
