package com.example.habb.pharmablock;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void inputPage(View view){

        Intent intent=new Intent(MainActivity.this,UploadActorData.class);
        startActivity(intent);
    }

    public void getchain(View view){

        Intent intent=new Intent(MainActivity.this,GetChain.class);
        startActivity(intent);
    }


    public void aboutpage(View view) {

        Intent intent=new Intent(MainActivity.this,AboutPage.class);
        startActivity(intent);

    }

    public void howTo(View view) {
        Intent intent=new Intent(MainActivity.this,HowTo.class);
        startActivity(intent);
    }
}
