package com.example.habb.pharmablock;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GetChain extends AppCompatActivity {

    private EditText searchID;
    String ID,Actor ;
    public Adapter adapter;
    private TextView mTextView;
    Handler handler;
    private ProgressBar mProgressBar;
    private   String url1="https://blockchain0.herokuapp.com/mine";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_chain);
//        getWindow().setBackgroundDrawableResource(R.drawable.back_a);


        searchID=findViewById(R.id.search_ID);
        mTextView= findViewById(R.id.test);
        mProgressBar= findViewById(R.id.progressBar5);

        mProgressBar.setVisibility(View.INVISIBLE);

        firstBoom();


    }

    private void firstBoom(){

        RequestQueue queue= Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url1,
                new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Toast.makeText(GetChain.this, "REsponse: " + response,Toast.LENGTH_SHORT).show();
                Log.i("Suckess", response.toString());
            }


                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }){

          };        queue.add(stringRequest);

    }


    public void GetChain(View view){

        GetChainz();

        if( mTextView.getText().equals("press the button")){

            GetChainz();

        }else {
            Toast.makeText(this, "chain found", Toast.LENGTH_SHORT).show();
        }
    }

    public void GetChainz(){

        ID = searchID.getText().toString();

        final String url="https://blockchain0.herokuapp.com/get_chain";

        RequestQueue queue= Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
               // Toast.makeText(GetChain.this, "REsponse: " + response, Toast.LENGTH_SHORT).show();
                Log.i("Suckess",response.toString());



                try {
                    JSONObject arr = new JSONObject(response);
                    JSONArray jObj = arr.getJSONArray("chain");
                    for (int i = 0; i < jObj.length(); i++) {
                        JSONObject mJsonObject = jObj.getJSONObject(i);
                        Log.i("trans2",mJsonObject.toString());





                        Log.d("OutPut", mJsonObject.getString("transactions"));


                        JSONObject arr2 =jObj.getJSONObject(i);
                        JSONArray jObj2 = arr2.getJSONArray("transactions");

                        for (int a = 0; a < jObj2.length(); a++) {
                            JSONObject mJsonObject2 = jObj2.getJSONObject(a);
                            Log.i("trans5",mJsonObject2.toString());

                            String ID2 = mJsonObject2.getString("drug_id");
                            String ID3 = mJsonObject2.getString("actor");



                                if (ID2.equals(ID) ) {
                                    mProgressBar.setVisibility(View.VISIBLE);
                                    final String deeds= jObj2.toString();
                                    Handler handler = new Handler();
                                    Runnable r = new Runnable() {
                                        public void run() {
                                            mProgressBar.setVisibility(View.INVISIBLE);
                                            mTextView.setText(deeds);
                                        }
                                    };

                                    handler.postDelayed(r, 2000);


                                    Log.d("OutPut1", mJsonObject2.getString("drug_id"));
                                }

                        }
                    }
                    Log.i("trans1",jObj.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){

        };        queue.add(stringRequest);





    }
}
